# PXE-booting Docker Host Farm Demo

## Introduction

This repository describes how to set up a farm of PXE-booting Alpine Linux
nodes. In this example, QEMU-based virtual machines are configured as Docker
hosts and use Alpine Linux in the run from RAM mode. Production systems would
likely use physical servers.

### Environment

* 2 virtual machines as redundant *inventory servers*
    * inventory of Docker hosts (contains hostnames, IP addresses & MAC addresses)
    * run services required to boot Docker hosts via network
* 2 virtual machines as Docker hosts
    * will boot via network (untagged VLAN)
    * no local OS will be installed on USB or local disk
    * local disk will be provisioned with swap and `/var` only
    * run-time services will be accessible via VLAN 42 only
* 2 virtual networks for redundancy
    * interconnected virtual switches
    * each VM connects to both networks
    * access to Internet (because the demo setup has no local Alpine Linux mirror nor Docker registry)

### IP Address Assignment

<table>
  <thead><tr><th>Addresses</th><th>VLAN</th><th>User</th></tr></thead>
  <tbody>
    <tr><td>10.10.0.1</td><td>Untagged</td><td>Virtual network gateway</td></tr>
    <tr><td>10.10.0.11</td><td>Untagged</td><td>VM #1 (Inventory)</td></tr>
    <tr><td>10.10.0.12</td><td>Untagged</td><td>VM #2 (Inventory)</td></tr>
    <tr>
      <td>10.10.0.100-149</td>
      <td>Untagged</td>
      <td>DHCP range for network boot phase, allocated by VM #1</td>
    </tr>
    <tr>
      <td>10.10.0.150-199</td>
      <td>Untagged</td>
      <td>DHCP range for network boot phase, allocated by VM #2</td>
    </tr>
    <tr><td>10.10.42.1</td><td>42</td><td>Virtual network gateway</td></tr>
    <tr><td>10.10.42.11</td><td>42</td><td>VM #1 (Inventory)</td></tr>
    <tr><td>10.10.42.12</td><td>42</td><td>VM #2 (Inventory)</td></tr>
    <tr><td>10.10.42.21</td><td>42</td><td>VM #3 (Docker host)</td></tr>
    <tr><td>10.10.42.22</td><td>42</td><td>VM #4 (Docker host)</td></tr>
  </tbody>
</table>

### Docker Host Boot Sequence

1. The host sends a DHCP discovery message via the first network interface. If there is no response, a similar message is sent via the second interface.
1. The host receives a DHCP offer from each available inventory on the active interface, called the Boot Interface (BI) hereafter.
1. The host accepts an offer from one inventory, called the Boot Server (BS) hereafter.
1. The host downloads PXELINUX from the BS via the BI using TFTP.
1. PXELINUX makes a TFTP request to the BS via the BI containing the MAC address of the BI, in order to obtain the relevant PXELINUX configuration file.
1. PXELINUX downloads the Alpine Linux kernel and initramfs from the BS via the BI using TFTP.
1. The initramfs downloads the modloop image from the BS via the BI using HTTP.
1. The `setup-disk` init script
    * initializes the local disk with swap and `/var` partitions
    * generates SSH host keys, and
    * deconfigures the BI.
1. Run-time network configuration takes place, including
    * creation of the bonding interface
    * configuration of VLAN 42 over the bonding interface, and
    * startup of a DHCP client on the said VLAN.
1. The DHCP client sends a DHCP discovery message via the VLAN interface.
1. The DHCP client receives a DHCP offer from each available inventory. Each offer contains the same static address that was looked up by the interface's MAC address.
1. The DHCP client accepts an offer from one inventory.
1. System services, including `dockerd`, start up normally.

## Demo System Setup

### Overlays

Build the overlays:

<pre>./build-apkovl.sh
</pre>

### Virtual Networks

Set up the virtual networks:

<pre>sudo ./network.sh create
</pre>

In this example, the public Docker registry and Alpine Linux mirrors are used,
so make sure that your host allows Internet access from the created `virbr0`,
`virbr1`, `bond0`, and `bond0.42` interfaces.

### Inventory VMs

Spin up the first inventory VM using an Alpine Linux ISO image:

<pre>./qemu.sh 1 -cdrom alpine-extended-3.12.2-x86_64.iso
</pre>

Use the following command line on the ISOLINUX boot loader, in order to enable
the virtual serial console:

<pre>lts console=ttyS0
</pre>

Log in as `root` and create a bootable partition on the virtual disk:

<pre>localhost:~# fdisk /dev/sda
Device contains neither a valid DOS partition table, nor Sun, SGI, OSF or GPT disklabel
Building a new DOS disklabel. Changes will remain in memory only,
until you decide to write them. After that the previous content
won't be recoverable.


Command (m for help): n
Partition type
   p   primary partition (1-4)
   e   extended
p
Partition number (1-4): 1
First sector (63-2097151, default 63):
Using default value 63
Last sector or +size{,K,M,G,T} (63-2097151, default 2097151):
Using default value 2097151

Command (m for help): t
Selected partition 1
Hex code (type L to list codes): c
Changed system type of partition 1 to c (Win95 FAT32 (LBA))

Command (m for help): a
Partition number (1-4): 1

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table
</pre>

Install Alpine Linux and create the APK cache:

<pre>mkfs.vfat /dev/sda1
modprobe vfat
setup-bootable /dev/cdrom /dev/sda1
mount /dev/sda1 /media/sda1
mkdir /media/sda1/cache
</pre>

Append the following to the `APPEND` line of
`/media/sda1/boot/syslinux/syslinux.cfg` to permanently enable the virtual
serial console:

<pre>console=ttyS0
</pre>

Encode the overlay on your host using Base64:

<pre>base64 inventory1.apkovl.tar.gz
</pre>

Install the overlay by running the following command on the VM, pasting the
encoded overlay to the console, and pressing Ctrl+D:

<pre>base64 -d > /media/sda1/inventory1.apkovl.tar.gz
</pre>

Reboot the VM:

<pre>reboot
</pre>

Log in as `root` again and finalize the installation:

<pre>lbu ci
apk update
apk cache sync
mount -w -o remount /media/sda1
mkinitfs -o /media/sda1/boot/initramfs-lts
poweroff
</pre>

Repeat the steps for the second inventory VM, substituting `2` for `1` when
running `qemu.sh` and using `inventory2.apkovl.tar.gz` as the overlay.


## Operating the Demo System

Once the virtual disks of the inventory VMs have been set up, you can spin up
the inventory VMs by running the following commands on separate terminals:

<pre>./qemu.sh 1
./qemu.sh 2
</pre>

Once the inventory VMs have started up, the host VMs can be launched too on
separate terminals:

<pre>./qemu.sh 3
./qemu.sh 4
</pre>

The VMs are accessible using SSH. For example, use the following command to
access VM #3:

<pre>ssh -i ssh-key root@10.10.42.21
</pre>

On the Docker hosts (VMs #3 and #4), you can deploy Docker containers:
<pre>docker1:~# docker run -d --restart always alpine sleep 10000
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
9aae54b2144e: Pull complete
Digest: sha256:826f70e0ac33e99a72cf20fb0571245a8fee52d68cb26d8bc58e53bfa65dcdfa
Status: Downloaded newer image for alpine:latest
0d181a7650e7d1d552487096295aa43aaf9b64c0781bd13d9b4948675fafbbb3
docker1:~# docker ps
CONTAINER ID   IMAGE     COMMAND         CREATED         STATUS         PORTS     NAMES
0d181a7650e7   alpine    "sleep 10000"   7 seconds ago   Up 4 seconds             happy_moore
</pre>

The SSH host keys and the `/var` directory are backed by the local disks of the
hosts. This means that even if the host VM was restarted, the deployed
container would be restarted too and the SSH host keys would remain unchanged.
Any Docker volumes would be preserved too.

This setup is resilient to failures. The Docker hosts would be able to boot
from network even if one inventory was shut down or one virtual switch was
down. The latter could be experimented with the following commands:

<pre>sudo ip link set virbr0 down
sudo ip link set virbr0 up
sudo ip link set virbr1 down
sudo ip link set virbr1 up
</pre>


## Remarks

Static IP address assignment is based on the mapping from MAC addresses to IP
addresses. Hence, an up-to-date mapping must be maintained in the
`/etc/dhcp/dhcpd.conf` files of the inventories.

A mapping from MAC addresses to virtual networks must be maintained too, in
order to implement highly available network boot. This is implemented as
symbolic links at `/usr/share/syslinux/pxelinux.cfg`, which point to
interface-specific PXELINUX configuration files, `eth0` and `eth1`. If the
Alpine Linux initramfs supported bonding, this would not be necessary, but now
it must be told which interface was able to reach the inventory and therefore
should be used to finish the boot process.

It is worth noting that the network boot process can get stuck if an inventory
becomes unreachable in the middle of the process or due to other transient
errors. This problem could be addressed in production systems by using hardware
watchdogs.
