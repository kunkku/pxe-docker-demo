#!/bin/sh -e

ovl() {
	echo $1.apkovl.tar.gz
}

build() {
	local server=$1
	shift

	local dir=$(mktemp -d)
	local layer
	for layer in base $* $server; do
		cp -a layers/$layer/* $dir
	done

	tar c -z -C $dir -f $(ovl $server) --owner 0 --group 0 $(ls $dir)
	rm -fr $dir
}

build host
FILE=$(ovl host)
VAR=layers/inventory/var
install -D -m 644 $FILE $VAR/www/localhost/htdocs/$FILE

build inventory1 inventory
build inventory2 inventory

rm -fr $FILE $VAR
