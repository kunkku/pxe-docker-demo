#!/bin/sh -ex

SLAVES="virbr0 virbr1"

if [ "$1" = create ]; then
	ip link add bond0 type bond mode broadcast
	ip link add isl0 type veth peer isl1

	for iface in $SLAVES; do
		isl=isl${iface#virbr}
		ip link add $iface type bridge
		ip link set $isl master $iface
		ip link set $iface master bond0
		ip link set $isl up
		ip link set $iface up
	done

	ip link set bond0 up
	ip address add 10.10.0.1/24 dev bond0

	ip link add bond0.42 link bond0 type vlan id 42
	ip link set bond0.42 up
	ip address add 10.10.42.1/24 dev bond0.42

elif [ "$1" = destroy ]; then
	for iface in isl0 $SLAVES bond0; do
		ip link del $iface
	done
fi
