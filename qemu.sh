#!/bin/sh

VM=$1
shift

IF_ARGS=
for i in 0 1; do
	IF_ARGS="$IF_ARGS \
		-netdev type=bridge,br=virbr$i,id=net$i \
		-device virtio-net-pci,netdev=net$i,mac=52:54:00:12:0$VM:0$i"
	if [ $VM -gt 2 ]; then
		IF_ARGS=${IF_ARGS},bootindex=$i
	fi
done

IMAGE=vm-$VM-disk.img
[ -e $IMAGE ] || qemu-img create $IMAGE 1G

exec qemu-kvm -m 1G -nographic $IF_ARGS "$@" $IMAGE
